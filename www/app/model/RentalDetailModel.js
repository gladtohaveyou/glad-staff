Ext.define('GladStaff.model.RentalDetailModel', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            { name: 'name', type: 'string' },
            { name: 'address', type: 'string' },
            { name: 'rentalStatus', type: 'string' },
            { name: 'statuses', type: 'auto'},
            { name: 'group', type: 'string'},
            { name: 'firstName', type: 'string'},
            { name: 'lastName', type: 'string'},
            { name: 'status', type: 'string'},
            { name: 'type', type: 'string'},
            { name: 'typeDescription', type: 'string'},
            { name: 'actionLabel', type: 'string'},
            { name: 'action', type: 'string'},
            { name: 'currentStatus', type: 'string'},
            { name: 'dataBlob', type: 'auto'},
            { name: 'sortOrder', type: 'int'},
            { name: 'caption', type: 'string'},
            { name: 'disclosure', type: 'bool'},
            { name: 'propertyId', type: 'int'}
        ]
    }
});

