Ext.define('GladStaff.view.MapView', {
    extend: 'Ext.Map',
    xtype: 'map',

    config: {
        id: 'unitMap',
        title: null,
        useCurrentLocation: false,
        mapOptions : {
            zoom: 15,
            center: new google.maps.LatLng(30.278982,-86.017023),
            mapTypeId : google.maps.MapTypeId.HYBRID,
            navigationControl: true,
            zoomControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.DEFAULT}
        }
    },
    addMarker: function(lat,lon,iconTitle) {
        var infoWindow = new google.maps.InfoWindow(),
            point = new google.maps.LatLng(
                lat,
                lon
            ),
            marker = new google.maps.Marker({
                map: this.getMap(),
                position: point
            });

        var infoContent = iconTitle + '<br/>';

        google.maps.event.addListener(marker, "click", function() {
            infoWindow.setContent(infoContent);
            infoWindow.open(this.getMap(), marker);
        });
    }
});
