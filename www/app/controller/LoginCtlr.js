Ext.define('GladStaff.controller.LoginCtlr', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            loginView: {
                selector: 'loginview',
                xtype: 'loginview',
                autoCreate: true
            },
            mainMenuView: 'mainview',
            mainTitleBarTitle: '#mainPanelTitle',
            mainListPanel: 'mainlistpanel'
        },
        control: {
            loginView: {
                signInCommand: 'onSignInCommand'
            },
            mainListPanel: {
                signOffCommand: 'onSignOffCommand'
            }
        }
    },
    index: function(args) {
        var auth = Ext.create('GladStaff.backend.Auth');

        auth.setXAPI('ADD2D74A-8F6B-40B3-9189-DEC54FE83E29');

        GladStaff.app.auth = auth;

        var view = this.getLoginView();
        Ext.Viewport.add(view);

        navigator.geolocation.getCurrentPosition(this.onGetLocation);
    },
    onGetLocation: function(position) {

        var location = position.coords.latitude + ',' + position.coords.longitude;
        GladStaff.app.auth.setXLOCATION(location);
        //GladStaff.app.auth.setXLOCATION('30.339923,-86.193535');
    },
    onSignInCommand: function (view, username, password) {

        GladStaff.app.showBusy();
        var me = this;
        var loginView = me.getLoginView();
    
        if (username.length === 0 || password.length === 0) {
            GladStaff.app.hideBusy();
            GladStaff.app.showError('Please enter your username and password.');
            return;
        }

        this.doLogin(username, password);

    },
    doLogin: function(username, password) {

        var me = this;
        GladStaff.backend.authentication({
            user: username,
            password: password
        }, {
            onSuccess: function(response, xhr, options) {
                GladStaff.app.hideBusy();
                me.signInSuccess(response);
            },
            onFailure: function(xhr, options) {
                GladStaff.app.hideBusy();
                GladStaff.app.showError('Login failed. Please try again.');
            },
            scope: this
        });
    },
    signInSuccess: function (response) {

        var loginView = this.getLoginView();
        loginView.setMasked(false);

        Ext.Array.forEach(response.properties, function(item, index, allItems){
            if (item.key == 'api')
            {
                GladStaff.app.auth.setXAPI(item.value);
            };
            if (item.key == 'auth_token'){
                GladStaff.app.auth.setXAUTHTOKEN(item.value);
            }
        });

        Ext.Viewport.remove(loginView,true);
        GladStaff.app.dispatch({
            controller: 'MainCtlr',
            action: 'index',
            args: [response]
        });
    },
    signInFailure: function (message) {
        var loginView = this.getLoginView();
        loginView.showSignInFailedMessage(message);
        loginView.setMasked(false);
    },
    onSignOffCommand: function () {
        Ext.Viewport.animateActiveItem(this.getLoginView(), GladStaff.app.getSlideRightTransition());
    }
});