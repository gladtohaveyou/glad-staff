Ext.define('GladStaff.view.RentalDetailActionView', {
    extend: 'GladStaff.view.BaseListView',
    xtype: 'rentaldetailactionview',

    requires: [
    ],
    config: {
        emptyText: 'No rental detail found',
        itemTpl: new Ext.XTemplate(
            '<tpl if="this.isDescription(group)">',
            '   <div>{description}</div>',
            '</tpl>',
            '<tpl if="this.isActions(group)">',
            '   <div class="action-label">{actionLabel}</div>',
            '</tpl>',
            {
                isDescription: function(group) {
                    if (group == 'Description')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                isActions: function(group) {
                    if (group == 'Actions')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        )
    }
});