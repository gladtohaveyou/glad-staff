Ext.define('GladStaff.view.EmployeeListView', {
    extend: 'GladStaff.view.BaseListView',
    xtype: 'employeelistpanel',

    requires: [
    ],
    config: {
        indexBar: true,
        emptyText: 'No Employees found...',
        itemTpl: new Ext.XTemplate(
            '<div>{name}</div>'
        )
    }
});