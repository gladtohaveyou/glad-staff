Ext.define('GladStaff.model.ReservationModel', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            { name: 'reservationId', type: 'int' },
            { name: 'externalId', type: 'string' },
            { name: 'type', type: 'string' },
            { name: 'typeDescription', type: 'string' },
            { name: 'status', type: 'string' },
            { name: 'arrival', type: 'string' },
            { name: 'departure', type: 'string' },
            { name: 'property', type: 'auto' },
            { name: 'primaryGuest', type: 'auto' },
            { name: 'invitedGuests', type: 'auto'},
            { name: 'doorCode', type: 'string' },
            { name: 'propertyAccessInstructions', type: 'string' },
            { name: 'actions', type: 'auto' }
        ]
    }
});
