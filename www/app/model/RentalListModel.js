Ext.define('GladStaff.model.RentalListModel', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            { name: 'name', type: 'string' },
            { name: 'distanceAway', type: 'string' },
            { name: 'street', type: 'string' },
            { name: 'inspStatusColor', type: 'string' },
            { name: 'hskStatusColor', type: 'string' },
            { name: 'maintStatusColor', type: 'string' }
        ]
    }
});
