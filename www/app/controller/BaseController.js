Ext.define('GladStaff.controller.BaseController', {
    extend: 'Ext.app.Controller',
    requires: [

    ],
    config: {
        refs: {
            mainView: 'mainview'
        },
        control: {

        }
    },

    //called when the Application is launched, remove if not needed
    launch: function(app) {

    },
    index: function(response) {

    },
    onGoBack: function() {
        var mainView = this.getMainView();
        mainView.pop();
    },
    doPush: function(pushView) {
        var mainView = this.getMainView();
        mainView.push(pushView);
    },
    doAction: function (record) {

        switch(record.data.action)
        {
            case 'glad_call':
                var link = 'tel:'+ record.data.dataBlob.data;
                var ref = window.open(encodeURI(link), '_blank', 'location=yes');
                break;
            case 'glad_email':
                var link = 'mailto:' + record.data.dataBlob.data;
                var ref = window.open(encodeURI(link), '_blank', 'location=yes');             
                break;
            case 'glad_text':
                var link = 'sms:' + record.data.dataBlob.data;
                var ref = window.open(encodeURI(link), '_blank', 'location=yes');
                break;
            case 'view_reservation':
                this.displayReservationDetail(record);
                break;
            case 'glad_view_content':
                this.displayRentalDetailActions(record);
                break;
            case 'map_unit':
                this.displayUnitMap(record);
                break;
            case 'view_unit_photos':
                this.displayUnitPhotos(record);
                break;
            case 'glad_view_url':
                var ref = window.open(encodeURI(record.data.dataBlob.data), '_self', 'location=yes');
                break;
            case 'glad_view_properties':
                this.displayRentalListDisclose(record);
                break;
            case 'view_area_list':
                this.displayAreaList(record);
                break;
            case 'glad_status_update':
                this.updateActions(record);
                break;
            case 'view_rentalreslistview':
                 this.displayReservationRentalList(record);
                break;
            case 'list_reservations':
                GladStaff.app.showBusy();
                GladStaff.app.dispatch({
                    controller: 'ReservationCtlr',
                    action: 'index',
                    args: [{apiKey:record.data.apiKey}]
                });
                break;
            case 'list_rentals':
                break;
            default:
                console.log('ERROR --> ' + record.data.action + ' needs to be configured')
                break;
        }
    },
    getSlideLeftTransition: function () {
        return { type: 'slide', direction: 'left' };
    },
    getSlideRightTransition: function () {
        return { type: 'slide', direction: 'right' };
    }
});
