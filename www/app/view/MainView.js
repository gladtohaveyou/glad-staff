Ext.define('GladStaff.view.MainView', {
    extend: 'Ext.NavigationView',
    xtype: 'mainview',

    config: {
        fullscreen:true,
        itemId: 'mainviewId',
        navigationBar: {
            items: [
                {
                    xtype: 'button',
                    text: 'Log Off',
                    itemId: 'logOffButton',
                    align: 'right'
                }
            ]
        },
        listeners: [{
            delegate: '#logOffButton',
            event: 'tap',
            fn: 'onLogOffButtonTap'
        }]
    },
    onLogOffButtonTap: function() {
        GladStaff.backend.auth = null;
        Ext.Viewport.remove(this,true);
        GladStaff.app.dispatch({
            controller: 'LoginCtlr',
            action: 'index',
            args: []
        });
    }
});
