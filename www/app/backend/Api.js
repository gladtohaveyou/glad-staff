Ext.define('GladStaff.backend.Api', {

    config: {
    },

    constructor: function(config) {
        this.initConfig(config);
    },

    authentication: function (params, callbacks) {
        this.postRequest({rest: '/api/authentication/staffsignin', jsonData: params}, callbacks);
    },
    getEmployeeList: function (callbacks) {
        this.getRequest('/staff/employee/all', callbacks);
    },
    getReservationList: function (args, callbacks) {
           this.getRequest('/staff/reservation/here/?id='+args.apiKey, callbacks);
     },
    getVendorList: function (callbacks) {
        this.getRequest('/staff/vendor/all', callbacks);
    },
    getCurrentRentalList: function (callbacks) {
        this.getRequest('/staff/property/current', callbacks);
    },
    doGetAction: function(path, callbacks) {
        this.getRequest(path, callbacks);
    },
    updateAction: function (params, callbacks) {
        this.postRequest({rest: '/staff/property/changestatus', jsonData: params}, callbacks);
    },
    baseUri: 'http://m.gladtohaveyou.local',
    //baseUri: 'http://10.0.1.100:801',

    request: function (options, callbacks) {
        var url = (/^http/.test(options.rest) ? '' : (options.baseUri || this.baseUri)) + options.rest;

        options.url = url;
        options.scope = (options.scope || this);

        Ext.applyIf(options, {
            dataType: 'json'
        });

        if (options.dataType === 'xml') {
            Ext.apply(options, {
                headers: {
                    'Content-Type': 'text/xml; charset=utf-8'
                }
            });
        } else {
            Ext.apply(options, {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept': 'application/json'
                }
            });
        }

        Ext.apply(options, {
            // disableCaching: false,
            success: function (xhr, options) {
                var response = xhr.responseText;

                if (options.dataType == "json" && response != "") {
                    try {
                        response = Ext.decode(response, true) || null;
                    } catch (e) {
                        console.log('Error while parsing json.');
                    }
                }

                callbacks.onSuccess.call(options.scope, response, xhr, options);
            },
            failure: function (xhr, options) {
                if (xhr.status === 401) {

                    GladStaff.app.dispatch({
                            controller: 'Login',
                            action: 'index',
                            args: [{forceLogin: true}]
                    });
                } else if (!Ext.isEmpty(xhr.responseText) || !Ext.isEmpty(xhr.statusText)) {
                    GladStaff.app.showMessage(xhr.responseText || '', xhr.statusText || '');
                } else {
                    GladStaff.app.showMessage('No network connection.');
                }
                callbacks.onFailure.call(options.scope, xhr, options);
            }
        });

        if (GladStaff.app.auth) {
            options.headers['X-API'] = GladStaff.app.auth.getXAPI();
        };
        if (GladStaff.app.auth && GladStaff.app.auth.getXAUTHTOKEN()) {
            options.headers['X-AUTH-TOKEN'] = GladStaff.app.auth.getXAUTHTOKEN();
        };
        options.headers['X-LOCATION'] = GladStaff.app.auth.getXLOCATION();

        Ext.Ajax.request(options);
    },
    /**
     * @private
     * Request function that uses HTTP GET method.
     */
    getRequest: function (rest, callbacks) {
        callbacks = this.prepareCallbacks(callbacks);
         this.request({
            'rest': rest,
            'method': 'GET',
            'scope': callbacks.scope
        }, callbacks);
    },
    /*
     * @private
     * Request function that uses HTTP POST method.
     */
    postRequest: function (options, callbacks) {
        callbacks = this.prepareCallbacks(callbacks);

        var requestOptions = {
                'method': 'POST',
                'scope': callbacks.scope
            },
            option;

        for (option in options) {
            if (options.hasOwnProperty(option)) {
                requestOptions[option] = options[option];
            }
        }

        this.request(requestOptions, callbacks);
    },
    /**
     * @private
     * Builds callback object with standard success/failure functions that are widely used by the API methods.
     */
    prepareCallbacks: function(callbacks) {
        var prepared = callbacks || {};

        prepared.onSuccess = prepared.onSuccess || Ext.emptyFn;
        prepared.onFailure = prepared.onFailure || Ext.emptyFn;

        return prepared;
    } 
},
    function(Backend) {
        Ext.onSetup(function() {
        // create global instance to access the service API
        // Sa.backend = new Backend();
        GladStaff.backend = Ext.apply(GladStaff.backend, new Backend());
    });
});