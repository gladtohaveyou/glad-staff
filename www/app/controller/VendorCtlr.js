Ext.define('GladStaff.controller.VendorCtlr', {
    extend: 'GladStaff.controller.BaseController',
    requires: [

    ],
    config: {
        refs: {
            vendorListPanel: {
                selector: 'vendorlistpanel',
                xtype: 'vendorlistpanel',
                autoCreate:true
            },
            vendorDetailView: {
                selector: 'vendordetailview',
                xtype: 'vendordetailview',
                autoCreate: true
            },
            vendorSearchBox:  'vendorlistpanel #search-field',
            listTitleBar: 'vendordetailview #list-title-bar'
        },
        control: {
            vendorSearchBox: {
                clearicontap: 'clearVendorSearch',
                keyup: 'vendorSearch'
            }
        }
    },

    //called when the Application is launched, remove if not needed
    launch: function(app) {

    },
    index: function(response) {
        this.getVendorListData();
    },
    onLoadVendorList: function(response) {

        var vendorListPanel = this.getVendorListPanel();
        var me = this;
        vendorListPanel.on({
            delegate: '#backButton',
            tap: function () {
                me.onGoBack();
            }
        });
        vendorListPanel.on({
            itemtap: function (list, index, target, record, e, eOpts) {
                me.onDisplayVendorDetail(list, record);
            }
        });
        var store = Ext.create('Ext.data.Store',
            {
                fields: ['name', 'notes', 'actions'],
                data: response
            }
        );

        store.setGrouper(function(record) {
            return record.get('name')[0];
        });

        vendorListPanel.setStore(store);
        vendorListPanel.setTitle('Vendors');

        this.doPush(vendorListPanel);
    },
    onLoadVendorListFailure: function(message) {
        console.log(message);
    },
    getVendorListData: function() {
        GladStaff.backend.getVendorList({
            onSuccess: function(response, xhr, options) {
                GladStaff.app.hideBusy();
                this.onLoadVendorList(response);
            },
            onFailure: function(xhr, options) {
                GladStaff.app.hideBusy();
                this.onLoadVendorListFailure('Cannot Load Vendor List.');
            },
            scope: this
        });
    },
    onDisplayVendorDetail: function(sender, record) {

        var detailView = this.getVendorDetailView();
        var me = this;
        detailView.on({
            delegate: '#backButton',
            tap: function () {
                me.onGoBack();
            }
        });
        detailView.on(
            {
                itemtap: function (list, index, target, record, e, eOpts) {
                    me.doAction(record);
                }
            }
        );

        var data = [
            {
                notes: record.data.notes,
                action: 'none',
                group: 'Notes',
                dataBlob: record.data,
                disclosure: false,
                sortOrder: 1
            }
        ];

        var actionData = null;
        if (record.data.actions) {
            actionData = record.data.actions.map(function (item, index) {
                return {
                    dataBlob: item,
                    actionLabel: item.title,
                    action: item.action,
                    group: 'Actions',
                    disclosure: true,
                    sortOrder: 3
                }
            });
        };

        if (actionData != null) {
            for (var i=0; i<actionData.length;i++)
            {
                data.push(actionData[i]);
            }
        };

        var store = Ext.create('Ext.data.Store',
            {
                fields: ['dataBlob','actionLabel','action','group','sortOrder','disclosure','notes'],
                data:data
            }
        );

        store.setGrouper (
            {
                sortProperty: 'sortOrder',
                property: 'group'
            }
        );

        detailView.setGrouped(true);
        detailView.setStore(store);
        detailView.setTitle(record.data.name);

        this.getListTitleBar().setHidden(true);

        this.doPush(detailView);
    },
    vendorSearch: function(searchField, e, options) {

        var searchValue = searchField.getValue();

        var vendorListPanel = this.getVendorListPanel();
        var thisRegEx = new RegExp(searchValue, 'i');

        var store = vendorListPanel.getStore();
        store.clearFilter();
        store.filterBy(function (record) {
            if (thisRegEx.test(record.get('name'))) {
                return true;
            };
            return false;
        });

    },
    clearVendorSearch: function(search, e, opts) {;
        var vendorListPane = this.getVendorListPanel();
        vendorListPane.getStore().clearFilter();
    }
});
