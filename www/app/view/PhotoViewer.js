Ext.define('GladStaff.view.PhotoViewer', {
    extend: 'Ext.Panel',
    xtype: 'photoviewer',

    requires: [

    ],
    config: {
        title:null,
        id:'images-view',
        fullscreen: true,
        layout: 'card',
        items: [
            {
                xtype: 'dataview',
                scrollable: 'vertical',
                itemId: 'photoViewerDataView',
                emptyText: 'No images to display',
                itemTpl: new Ext.XTemplate(
                        '<figure class="thumb-wrap">',
                            '<div class="thumb"><img src="{thumbnail}" title="{caption}"></div>',
                            '<figcaption class="captionText">{caption}</figcaption>',
                        '</figure>')
            }
        ]
    }
});
