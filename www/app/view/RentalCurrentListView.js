Ext.define('GladStaff.view.RentalCurrentListView', {
    extend: 'GladStaff.view.BaseListView',
    xtype: 'currentrentallistpanel',

    requires: [
        'Ext.TitleBar'
    ],
    config: {
        emptyText: 'No area information found...',
        itemTpl: new Ext.XTemplate(
                '<div class="rental-category-title">{title:ellipsis(35,true)}</div>'
        )
    }
});