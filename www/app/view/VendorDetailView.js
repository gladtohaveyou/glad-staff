Ext.define('GladStaff.view.VendorDetailView', {
    extend: 'GladStaff.view.BaseListView',
    xtype: 'vendordetailview',

    requires: [
    ],
    config: {
        indexBar: false,
        itemTpl:[
            '<tpl if="this.isNotes(group)">',
                '<div class="notes">{notes}</div>',
            '</tpl>',
            '<tpl if="this.isActions(group)">',
                '<div class="action-label">{actionLabel}</div>',
            '</tpl>',
            {
                isNotes: function(group) {
                    if (group == 'Notes')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                isActions: function(group) {
                    if (group == 'Actions')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

        ]
    }
});