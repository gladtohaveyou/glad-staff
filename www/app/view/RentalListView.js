Ext.define('GladStaff.view.RentalListView', {
    extend: 'GladStaff.view.BaseListView',
    xtype: 'rentallist',

    requires: [
    ],
    config: {
        emptyText: 'No rental units found...',
        grouped:false,
        itemTpl: new Ext.XTemplate(
            '<tpl for=".">',
            '<div>I<span class="icon-detail-action-{inspStatusColor}"></span>H<span class="icon-detail-action-{hskStatusColor}"></span>M<span class="icon-detail-action-{maintStatusColor}"></span></div><div class="rental-name">{name}</div><div class="rental-address">{street}</div><div class="rental-distance">{distanceAway}</div>',
            '</tpl>'
        )
    }
});