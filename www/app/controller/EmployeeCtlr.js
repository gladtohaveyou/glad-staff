Ext.define('GladStaff.controller.EmployeeCtlr', {
    extend: 'GladStaff.controller.BaseController',

    config: {
        refs: {
            mainListPanel: 'mainlistpanel',
            employeeListPanel: {
                selector: 'employeelistpanel',
                xtype: 'employeelistpanel',
                autoCreate:true
            },
            employeeDetailView: {
                selector: 'employeedetailview',
                xtype: 'employeedetailview',
                autoCreate: true
            },
            employeeSearchField:  'employeelistpanel #search-field',
            listTitleBar: 'employeedetailview #list-title-bar'
        },
        control: {
                employeeSearchField: {
                    clearicontap: 'clearEmployeeSearch',
                    keyup: 'employeeSearch'
                }
        }
    },

    //called when the Application is launched, remove if not needed
    launch: function(app) {

    },
    index: function(response) {
        this.getEmployeeList();
    },
    onLoadEmployeeList: function(response) {

        var me = this;
        var employeeListPanel = this.getEmployeeListPanel();
        employeeListPanel.on({
            delegate: '#backButton',
            tap: function () {
                me.doGoBack();
            }
        });
        employeeListPanel.on({
            itemtap: function (list, index, target, record, e, eOpts) {
                me.onEmployeeDetailView(record);
            }
        });

        var store = Ext.create('Ext.data.Store',
            {
                fields: ['name','canChangePropertyStatus','membersOf','actions'],
                data: response
            }
        );

        store.setGrouper(function(record) {
            return record.get('name')[0];
        });
        store.setData(response);
        employeeListPanel.setStore(store);
        employeeListPanel.setTitle('Employees');

        this.doPush(employeeListPanel);
    },
    onLoadEmployeeListFailure: function(message) {
        console.log(message);
    },
    getEmployeeList: function() {
        GladStaff.backend.getEmployeeList({
            onSuccess: function(response, xhr, options) {
                GladStaff.app.hideBusy();
                this.onLoadEmployeeList(response);
            },
            onFailure: function(xhr, options) {
                GladStaff.app.hideBusy();
                this.onLoadEmployeeListFailure('Cannot Load Employee List.');
            },
            scope: this
        });
    },
    onEmployeeDetailView: function(record) {

        var detailView = this.getEmployeeDetailView();
        var me = this;
        detailView.on({
            delegate: '#backButton',
            tap: function () {
                me.onGoBack();
            }
        });
        detailView.on(
            {
                itemtap: function (list, index, target, record, e, eOpts) {
                    me.doAction(record);
                }
            }
        );

        var data = [];

        var actionData = null;
        if (record.data.actions) {
            actionData = record.data.actions.map(function (item, index) {
                return {
                    dataBlob: item,
                    actionLabel: item.title,
                    action: item.action,
                    group: 'Actions',
                    disclosure: false,
                    sortOrder: 3
                }
            });
        };

        if (actionData != null) {
            for (var i=0; i<actionData.length;i++)
            {
                data.push(actionData[i]);
            }
        };

        var store = Ext.create('Ext.data.Store',
            {
                fields: ['dataBlob','actionLabel','action','group','sortOrder','disclosure'],
                data:data
            }
        );

        store.setGrouper (
            {
                sortProperty: 'sortOrder',
                property: 'group'
            }
        );

        detailView.setGrouped(true);
        detailView.setStore(store);
        detailView.setTitle(record.data.name);

        this.getListTitleBar().setHidden(true);
        this.doPush(detailView);
    },
    employeeSearch: function(searchField, e, options) {

        var searchValue = searchField.getValue();

        var employeeListPanel = this.getEmployeeListPanel();

        var store = employeeListPanel.getStore();
        store.clearFilter();

        var thisRegEx = new RegExp(searchValue, 'i');
        store.filterBy(function (record) {
            if (thisRegEx.test(record.get('name'))) {
                return true;
            };
            return false;
        });

    },
    clearEmployeeSearch: function(search, e, opts) {
        var employeeListPane = this.getEmployeeListPanel();
        employeeListPane.getStore().clearFilter();
    }
});
