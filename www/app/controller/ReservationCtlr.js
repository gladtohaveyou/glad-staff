Ext.define('GladStaff.controller.ReservationCtlr', {
    extend: 'GladStaff.controller.BaseController',

    config: {
        refs: {
            mainView : 'mainview',
            reservationListPanel: {
                selector: 'reservationlistpanel',
                xtype: 'reservationlistpanel',
                autoCreate:true
            },
            reservationDetailView: {
                selector: 'reservationdetailview',
                xtype: 'reservationdetailview',
                autoCreate: true
            },

            noteList: '#noteList',
            actionList: '#actionList',
            rentalDetailView: 'rentalDetailView',
            resSearchBox:  'reservationlistpanel #search-field',
            listTitleBar: 'reservationdetailview #list-title-bar'
        },
        control: {
            resSearchBox: {
                clearicontap: 'clearResSearch',
                keyup: 'resSearch'
            }
        }
    },

    //called when the Application is launched, remove if not needed
    launch: function(app) {

    },
    index: function(apiKey) {
        this.getReservationList(apiKey);
    },
    onLoadReservationList: function(response) {

        if (this.getReservationListPanel())
        {
            this.getReservationListPanel().destroy();
        }
        var me = this;
        var reservationListPanel = this.getReservationListPanel();
        reservationListPanel.on({
            delegate: '#backButton',
            tap: function () {
                me.onGoBack();
            }
        });
        reservationListPanel.on({
            itemtap: function (list, index, target, record, e, eOpts) {
                me.onDisplayReservationDetail(record);
            }
        });

        var store = Ext.create('Ext.data.Store', {model: 'GladStaff.model.ReservationModel',data:response});
        store.sort('departure', 'ASC');

        reservationListPanel.setStore(store);
        reservationListPanel.setTitle('Reservations');

        this.doPush(reservationListPanel);

    },
    onLoadReservationListFailure: function(message) {
        console.log(message);
    },
    getReservationList: function(apiKey) {
        GladStaff.backend.getReservationList(apiKey,{
            onSuccess: function(response, xhr, options) {
                GladStaff.app.hideBusy();
                this.onLoadReservationList(response);
            },
            onFailure: function(xhr, options) {
                GladStaff.app.hideBusy();
                this.sessionToken = null;
                this.showMessage('Cannot Load Reservation List.');
            },
            scope: this
        });
    },
    onDisplayReservationDetail: function(record) {

        var detailView = this.getReservationDetailView();
        var me = this;
        detailView.on({
            delegate: '#backButton',
            tap: function () {
                me.onGoBack();
            }
        });
        detailView.on(
            {
                itemtap: function (list, index, target, record, e, eOpts) {
                    me.doAction(record);
                }
            }
        );
        var data = [
            {
                type: record.data.type,
                typeDescription: record.data.typeDescription,
                status: record.data.status,
                arrival: record.data.arrival,
                departure: record.data.departure,
                isvip: record.data.primaryGuest.isvip,
                action: 'none',
                group: 'Guest Info',
                disclosure: false,
                sortOrder: 1
            }
        ];

        var propData =  {
                doorCode: record.data.doorCode,
                propertyAccessInstructions: record.data.propertyAccessInstructions,
                action: 'none',
                group: 'Property Info',
                disclosure: false,
                sortOrder: 2
        };
        data.push(propData);

        var notesData = null;
        if (record.data.primaryGuest.notes) {
            notesData = record.data.primaryGuest.notes.map(function (item, index) {
                return {
                    dataBlob: item,
                    title: item.title,
                    text: item.text,
                    group: 'Notes',
                    disclosure: false,
                    sortOrder: 3
                }
            });
        };

        var actionData = null;
        if (record.data.actions) {
            actionData = record.data.actions.map(function (item, index) {
                return {
                    dataBlob: item,
                    actionLabel: item.title,
                    action: item.action,
                    group: 'Actions',
                    disclosure: false,
                    sortOrder: 10
                }
            });
        };

        var invitedGuests = null;
        if (record.data.invitedGuests) {
            invitedGuests = record.data.invitedGuests.map(function (item, index) {
                return {
                    fullName: item.fullName,
                    email: item.email,
                    isvip: item.isvip,
                    group: 'Invited Guests',
                    disclosure: false,
                    sortOrder: 6
                }
            });
        };

        if (notesData != null) {
            for (var i=0; i<notesData.length;i++)
            {
                data.push(notesData[i]);
            }
        };

        if (actionData != null) {
            for (var i=0; i<actionData.length;i++)
            {
                data.push(actionData[i]);
            }
        };

        if (invitedGuests != null) {
            for (var i=0; i<invitedGuests.length;i++)
            {
                data.push(invitedGuests[i]);
            }
        };

        var store = Ext.create('Ext.data.Store',
            {
                fields: ['doorCode','propertyAccessInstructions','action','group','disclosure','sortOrder',
                         'dataBlob','actionLabel','action','fullName','email','isvip','status','arrival', 'departure',
                        'typeDescription','title','text','type'
                        ],
                data:data
            }
        );

        store.setGrouper (
            {
                sortProperty: 'sortOrder',
                property: 'group'
            }
        );

        detailView.setGrouped(true);
        detailView.setStore(store);
        detailView.setTitle(record.data.primaryGuest.fullName);

        this.getListTitleBar().setHidden(true);

        this.doPush(detailView);
    },
    resSearch: function(searchField, e, options) {

        var searchValue = searchField.getValue();

        var resListPanel = this.getReservationListPanel();
        var thisRegEx = new RegExp(searchValue, 'i');

        var store = resListPanel.getStore();
        store.clearFilter();
        store.filterBy(function (record) {
            var pg = record.get('primaryGuest');
            if (thisRegEx.test(pg.fullName)) {
                return true;
            };
            return false;
        });

    },
    clearResSearch: function(search, e, opts) {;
        var resListPanel = this.getReservationListPanel();
        resListPanel.getStore().clearFilter();
    }
});
