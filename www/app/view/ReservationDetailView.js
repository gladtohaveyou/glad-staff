Ext.define('GladStaff.view.ReservationDetailView', {
    extend: 'GladStaff.view.BaseListView',
    xtype: 'reservationdetailview',

    requires: [

    ],
    config: {
        emptyText: 'No reservation detail found',
        itemTpl:[
            '<tpl if="this.isGuestInfo(group)">',
                '<div class="reservation-status">{status}</div>',
                '<div class="{type:this.isGuestOwner}">{typeDescription}</div>',
                "<div class='arrival-date'>Arriving: {arrival:date('m/d/Y')}</div>",
                "<div class='departure-date'>Departing: {departure:date('m/d/Y')}</div>",
            '</tpl>',
            '<tpl if="this.isPropInfo(group)">',
                '<div class="door-access-code">Access: {doorCode}</div>',
                '<div class="prop-access-ins">{propertyAccessInstructions}</div>',
            '</tpl>',
            '<tpl if="this.isNotes(group)">',
                '<div class="note-title">{title}</div>',
                '<div class="note-text">{text}</div>',
            '</tpl>',
            '<tpl if=this.isInvitedGuests(group)">',
                '<div class="full-name">{fullName}</div>',
                '<div class="email">{email}</div>',
            '</tpl>',
            '<tpl if="this.isActions(group)">',
                '<div class="action-label">{actionLabel}</div>',
            '</tpl>',
            {
                isGuestInfo: function(group) {
                    if (group == 'Guest Info')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                isPropInfo: function(group) {
                    if (group == 'Property Info')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                isInvitedGuests: function(group) {
                    if (group == 'Invited Guests')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                isNotes: function(group) {
                    if (group == 'Notes')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                isActions: function(group) {
                    if (group == 'Actions')
                    {
                        return true;
                    }
                    else
                    {
                         return false;
                    }
                },
                isGuestOwner: function(type) {
                    if (type == 'owner')
                    {
                        return 'guest-name-alert'
                    }
                    else
                    {
                        return 'guest-name-normal'
                    }
                }
            }
        ]
    }
});