Ext.define('GladStaff.view.ReservationListView', {
    extend: 'GladStaff.view.BaseListView',
    xtype: 'reservationlistpanel',

    requires: [

    ],
    config: {
        title:null,
        emptyText: 'No reservations found...',
        grouped:false,
        itemTpl: new Ext.XTemplate(
            '<div class="{type:this.isGuestOwner}">{primaryGuest.lastName}, {primaryGuest.firstName}</div>',
            '<div class="rental-property-name">{property.name}</div>',
            '<div class="rental-status-normal">{status}</div>',
            '<div class="{type:this.isGuestOwner}">{typeDescription}</div>',
            {
                isGuestOwner: function(type) {
                    if (type == 'owner')
                    {
                        return 'guest-name-alert'
                    }
                    else
                    {
                        return 'guest-name-normal'
                    }
                }
            }
        )
    }
});