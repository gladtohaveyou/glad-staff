Ext.define('GladStaff.view.VendorListView', {
    extend: 'GladStaff.view.BaseListView',
    xtype: 'vendorlistpanel',

    requires: [
    ],
    config: {
        indexBar: true,
        emptyText: 'No vendors found',
        itemTpl: new Ext.XTemplate(
            '<div>{name}</div>'
        )
    }
});