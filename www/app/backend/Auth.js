Ext.define('GladStaff.backend.Auth', {
    config: {
        XAPI: null,
        XAUTHTOKEN: null,
        XAREAAPI:null,
        XLOCATION: null
    },
    constructor: function(config) {
        this.initConfig(config);
    }
});