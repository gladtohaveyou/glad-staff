Ext.define('GladStaff.view.RentalDetailView', {
    extend: 'GladStaff.view.BaseListView',
    xtype: 'rentaldetailview',

    requires: [
    ],
    config: {
        propertyDetailPath: null,
        emptyText: 'No rental detail found',
        itemId: 'rental-detail-list',
        itemTpl: new Ext.XTemplate(
            '<tpl if="this.isUnitInfo(group)">',
                '<div class="rental-name">{name}</div><div class="rental-address">{address}</div><div class="{rentalStatus:this.getStatusColor}">{rentalStatus}</div>',
            '</tpl>',
            '<tpl if="this.isCurrentRentalInfo(group)">',
                '<div class="{type:this.isGuestOwner}">{firstName} {lastName}</div><div class="guest-status">{status}</div><div class="guest-type">{typeDescription}</div>',
            '</tpl>',
            '<tpl if="this.isUpcomingRentals(group)">',
                '<div class="{type:this.isGuestOwner}">{firstName} {lastName}</div><div class="guest-status">{status}</div><div class="guest-type">{typeDescription}</div>',
            '</tpl>',
            '<tpl if="this.isActions(group)">',
            '<div class="icon-action-{currentStatus:this.getActionTitleColor}"><span class="action-label">{actionLabel}</span></div>',
            '</tpl>',
            '<tpl if="this.isStatus(group)">',
            '<div class="icon-action-{currentStatus:this.getActionTitleColor}"><span class="action-label">{actionLabel}</span></div>',
            '</tpl>',
            {
                isUnitInfo: function(group) {
                    if (group == 'Unit Info')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                isCurrentRentalInfo: function(group) {
                    if (group == 'Current or Next Reservation')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                isUpcomingRentals: function(group) {
                    if (group == 'Upcoming Reservation')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                isActions: function(group) {
                    if (group == 'Actions')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                isStatus: function(group) {
                if (group == 'Status')
                {
                    return true;
                }
                else
                {
                    return false;
                }
            },
                getStatusColor: function(status) {
                    if (status == 'Owner Occupied')
                    {
                        return 'rental-status-alert'
                    }
                    else
                    {
                        return 'rental-status-normal'
                    }
                },
                isGuestOwner: function(type) {
                    if (type == 'owner')
                    {
                        return 'guest-name-alert'
                    }
                    else
                    {
                        return 'guest-name-normal'
                    }
                },
                getActionTitleColor: function(status) {
                    if (status == 'red' || status == 'yellow' || status == 'green')
                    {
                        return status;
                    }
                    else
                    {
                        if (status == 'na')
                        {
                            return 'white';
                        }
                        else
                        {
                            return 'normal';
                        }
                    }
                }
            }
        )
    }
});