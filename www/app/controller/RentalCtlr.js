Ext.define('GladStaff.controller.RentalCtlr', {
    extend: 'GladStaff.controller.BaseController',

    config: {
        refs: {
            currentRentalListPanel: {
                selector: 'currentrentallistpanel',
                xtype: 'currentrentallistpanel',
                autoCreate: true
            },
            rentalResListView: {
                selector: 'rentalreslistview',
                xtype: 'rentalreslistview',
                autoCreate: true
            },
            currentAreaDV: '#currentAreaDV',
            otherAreaDV: '#otherAreaDV',

            rentalCategoryListPanel: {
                selector: 'rentalcategorylistpanel',
                xtype: 'rentalcategorylistpanel',
                autoCreate: true
            },
            rentalList: {
                selector: 'rentallist',
                xtype: 'rentallist',
                autoCreate: true
            },
            mainListPanel: 'mainlistpanel',
            rentalDetailView: {
                selector: 'rentaldetailview',
                xtype: 'rentaldetailview',
                autoCreate: true
            },
            rentalGuestDetail: '#rentalGuestDetail',
            rentalUpcomingGuests: '#rentalUpcomingGuests',
            reservationDetailView: {
                selector: 'reservationdetailview',
                xtype: 'reservationdetailview',
                autoCreate: true
            },
            map: {
                selector:'map',
                xtype: 'map',
                autoCreate: true
            },
            rentalDetailActionView: {
                selector: 'rentaldetailactionview',
                xtype: 'rentaldetailactionview',
                autoCreate: true
            },
            photoViewer: {
                selector: 'photoviewer',
                xtype: 'photoviewer',
                autoCreate: true
            },
            photoViewerDataView: '#photoViewerDataView',
            photoCarouselView: {
                selector: 'photocarouselview',
                xtype: 'photocarouselview',
                autoCreate: true
            },
            searchBox:  'currentrentallistpanel #search-field',
            resRentalSeachBox: '',
            rentalSearchBox: 'rentallist #search-field',
            areaSearchBox: 'rentalcategorylistpanel #search-field',
            listTitleBar: 'rentaldetailview #list-title-bar',
            currentRentalListTitleBar: 'currentrentallistpanel #list-title-bar',
            listTitleBarAction : 'rentaldetailactionview #list-title-bar',
            listTitleBarRes: 'reservationdetailview #list-title-bar',
            rentalDetailList: '#rental-detail-list'
        },
        control: {
            searchBox: {
                clearicontap: 'clearSearch',
                keyup: 'search'
            },
            rentalSearchBox: {
                clearicontap: 'clearRentalSearch',
                keyup: 'searchRentals'
            },
            areaSearchBox: {
                clearicontap: 'clearAreaSearch',
                keyup: 'searchAreas'
            }
        }
    },

    //called when the Application is launched, remove if not needed
    launch: function(app) {

    },
    index: function() {
        this.getCurrentRentalListData();
    },

    // Get Data For Current Areas and Other Areas
    getCurrentRentalListData: function() {
        GladStaff.backend.getCurrentRentalList({
            onSuccess: function(response, xhr, options) {
                GladStaff.app.hideBusy();
                this.displayCurrentRentalList(response);
            },
            onFailure: function(xhr, options) {
                GladStaff.app.hideBusy();
                this.showMessage('Cannot Load Reservation List.');
            },
            scope: this
        });
    },

    displayReservationRentalList: function(response) {

        var rentalResListView = this.getRentalResListView();
        var me = this;
        rentalResListView.on(
            {
                delegate: '#backButton',
                tap: function () {
                    me.onGoBack();
                }
            }
        );
        rentalResListView.on(
            {
                itemtap: function (list, index, target, record, e, eOpts) {
                    me.doAction(record);
                }
            }
        );

        var data = response.data.categories.map(function (item, index){
            return {
                title: item.category,
                action: item.action,
                group: 'Categories',
                totalProperties: item.totalProperties,
                path: item.path,
                sortOrder: 2
            };
        });
        var data2 =
            {
                title: 'View Reservations',
                action: 'list_reservations',
                group: 'Reservations',
                apiKey: response.data.apiKey,
                sortOrder: 1
            };

        data.push(data2);

        var store = Ext.create('Ext.data.Store', {model: 'GladStaff.model.CurrentRentalModel',data:data});

        store.setGrouper (
            {
                sortProperty: 'sortOrder',
                property: 'group'
            }
        );

        store.sort('title');

        rentalResListView.setGrouped(true);
        rentalResListView.setStore(store);
        rentalResListView.setTitle(response.data.title);

        this.doPush(rentalResListView);

    },
    // Build Current Area and Other Areas
    displayCurrentRentalList: function(response) {

        var currentRentalListPanelView = this.getCurrentRentalListPanel();
        this.getCurrentRentalListTitleBar().setHidden(true);

        var me = this;
        currentRentalListPanelView.on(
            {
                delegate: '#backButton',
                tap: function () {
                    me.onGoBack();
                }
            }
        );
        currentRentalListPanelView.on(
            {
                itemtap: function (list, index, target, record, e, eOpts) {
                    me.doAction(record);
                }
            }
        );

        var data = [
            {
                title: 'View Reservations',
                action: 'list_reservations',
                group: 'Current Area: '+ response.currentArea.name,
                sortOrder: 1
            },
            {
                title: 'View Unit Categories',
                action: 'view_area_list',
                group: 'Current Area: '+ response.currentArea.name,
                categories: response.currentArea.categories,
                apiKey: response.currentArea.apiKey,
                sortOrder: 1
            }
        ];

        var dataOther = response.otherAreas.map(function (item, index){
            return {
                title: item.name,
                action: 'view_rentalreslistview',
                group: 'Other Areas',
                categories: item.categories,
                sortOrder: 3,
                apiKey: item.apiKey,
                dataBlog: response.otherAreas
            };
        });

        for (var i=0; i<dataOther.length;i++)
        {
            data.push(dataOther[i]);
        };

        var store = Ext.create('Ext.data.Store', {model: 'GladStaff.model.CurrentRentalModel',data:data});

        store.setGrouper (
            {
                sortProperty: 'sortOrder',
                property: 'group'
            }
        );

        currentRentalListPanelView.setGrouped(true);
        currentRentalListPanelView.setStore(store);
        currentRentalListPanelView.setTitle('Rentals');

        this.doPush(currentRentalListPanelView);
    },

    // Get All Rental Properties for a given Area
    getAllRentalListData: function(path) {
        GladStaff.backend.doGetAction(path, {
            onSuccess: function(response, xhr, options) {
                GladStaff.app.hideBusy();
                this.displayRentalList(response);
            },
            onFailure: function(xhr, options) {
                GladStaff.app.hideBusy();
                this.showMessage('Cannot load rental list.');
            },
            scope: this
        });
    },

    // Display Rental List with ItemTap for Detail
    displayRentalList: function (response) {
        var propertyListView = this.getRentalList();
        var me = this;
        propertyListView.on(
            {
                delegate: '#backButton',
                tap: function () {
                    me.onGoBack();
                }
            }
        );
        propertyListView.on(
            {
                itemtap: function (list, index, target, record, e, eOpts) {
                    me.displayRentalUnitDetail(record.data.path);
                }
            }
        );

        var dataItem = null;

        var i = 0;
        var data = [];

        Ext.Array.forEach(response, function(mainItem, index, allItems) {
            var dataItem = {
                name: mainItem.name,
                street: mainItem.address.street,
                distanceAway: mainItem.distanceAway,
                inspStatusColor: "white",
                hskStatusColor: "white",
                maintStatusColor: "white",
                path: mainItem.path
            };

            Ext.Array.forEach(mainItem.propertyStatus, function(item, index, allItems){
                if (item.category == 'Housekeeping' && item.currentStatus != 'na')
                {
                    dataItem.hskStatusColor = item.currentStatus;
                };
                if (item.category == 'QA' && item.currentStatus != 'na')
                {
                    dataItem.inspStatusColor = item.currentStatus;
                };
                if (item.category == 'Maintenance' && item.currentStatus != 'na')
                {
                    dataItem.maintStatusColor = item.currentStatus;
                };
            });

            data.push(dataItem);

        });

        propertyListView.setData(data);
        propertyListView.getStore().sort('name');
        propertyListView.setTitle('Units');

        this.doPush(propertyListView);
    },

    // Get Specific Rental Data
    getRentalData: function(path) {
        GladStaff.backend.doGetAction(path, {
            onSuccess: function(response, xhr, options) {
                GladStaff.app.hideBusy();
                this.displayRentalInformation(response, path);
            },
            onFailure: function(xhr, options) {
                GladStaff.app.hideBusy();
                this.showMessage('Cannot load rental');
            },
            scope: this
        });
    },

    // Display Rental Detail Information
    displayRentalInformation: function(response, path){

        var rentalDetailView = this.getRentalDetailView();
        rentalDetailView.setPropertyDetailPath(path);

        var me = this;

        rentalDetailView.on(
            {
                delegate: '#backButton',
                tap: function () {
                    me.onGoBack();
                }
            }
        );
        rentalDetailView.on(
            {
                itemtap: function (list, index, target, record, e, eOpts) {
                    me.doAction(record);
                }
            }
        );

        var data = this.buildRentalData(response);

        var store = Ext.create('Ext.data.Store', {model: 'GladStaff.model.RentalDetailModel',data:data});

        store.setGrouper(
            {
                sortProperty: 'sortOrder',
                property: 'group'
            }
        )

        rentalDetailView.setGrouped(true);
        rentalDetailView.setStore(store);
        rentalDetailView.setTitle(response[0].name);

        this.getListTitleBar().setHidden(true);

        this.doPush(rentalDetailView);

    },

    // Build Actions and Screen Data
    buildRentalData: function(response) {

        var data = [
            {
                name: response[0].name,
                action: 'view_unitInfo',
                group: 'Unit Info',
                rentalStatus: response[0].status,
                dataBlob: response[0],
                disclosure: false,
                sortOrder: 0
            }
        ];

        var currentRes = null;
        if (response[0].currentReservation)
        {
            currentRes =
                {

                    firstName: response[0].currentReservation.primaryGuest.firstName,
                    lastName: response[0].currentReservation.primaryGuest.lastName,
                    status: response[0].currentReservation.status,
                    type: response[0].currentReservation.type,
                    typeDescription: response[0].currentReservation.typeDescription,
                    action: 'view_reservation',
                    group: 'Current or Next Reservation',
                    //dataBlob: response[0].currentReservation,
                    dataBlob: response[0],
                    disclosure: true,
                    sortOrder: 1
                }
        };

        var upCommingRes = null;
        if (response[0].upcomingReservations) {
            upCommingRes = response[0].upcomingReservations.map(function (item, index) {
                return {
                    firstName: item.primaryGuest.firstName,
                    lastName: item.primaryGuest.lastName,
                    status: item.status,
                    type: item.type,
                    typeDescription: item.typeDescription,
                    dataBlob: item,
                    action: 'view_reservation',
                    group: 'Upcoming Reservation',
                    disclosure: true,
                    sortOrder: 2
                }
            });
        };

        var actionData = null;
        if (response[0].actions) {
            actionData = response[0].actions.map(function (item, index) {
                return {
                    dataBlob: item,
                    actionLabel: item.title,
                    action: item.action,
                    group: 'Actions',
                    currentStatus: 'none',
                    disclosure: true,
                    propertyId: response[0].id,
                    sortOrder: 4
                }
            });
        };

        var propStatuses = null;
        if (response[0].statuses) {

            propStatuses = response[0].statuses.map(function (item, index) {
                console.log('currentStatus', item.currentStatus);
                    return {
                        dataBlob: item,
                        actionLabel: item.title,
                        action: item.action,
                        currentStatus: item.currentStatus,
                        group: 'Status',
                        disclosure: true,
                        propertyId: response[0].id,
                        sortOrder: 6
                    }
            })
        };

        data.push(
            {
                dataBlob:response[0].address,
                actionLabel: 'Map',
                action:'map_unit',
                group: 'Actions',
                disclosure: true,
                sortOrder: 4
            }
        );

        if (response[0].photos)
        {
            data.push(
                {
                    dataBlob:response[0].photos,
                    actionLabel: 'Photos',
                    action:'view_unit_photos',
                    group: 'Actions',
                    disclosure: true,
                    sortOrder: 4
                }
            )
        };

        if (upCommingRes != null)
        {
            for (var i=0; i<upCommingRes.length;i++)
            {
                data.push(upCommingRes[i]);
            }
        };

        if (actionData != null) {
            for (var i=0; i<actionData.length;i++)
            {
                data.push(actionData[i]);
            }
        };

        if (propStatuses != null)
        {
            for (var i=0; i<propStatuses.length;i++)
            {
                if (propStatuses[i].currentStatus != 'na') {
                    data.push(propStatuses[i]);
                }
            }
        };

        if (currentRes != null) {
            data.push(currentRes);
        };

        return data;
    },

    getRentalDetailActions: function(path) {
        GladStaff.backend.doGetAction(path, {
            onSuccess: function(response, xhr, options) {
                GladStaff.app.hideBusy();
                this.displayRentalDetailActionInformation(response);
            },
            onFailure: function(xhr, options) {
                GladStaff.app.hideBusy();
                this.showMessage('Cannot load rental');
            },
            scope: this
        });
    },

    displayRentalDetailActionInformation: function(response) {

        var rentalDetailActionView = this.getRentalDetailActionView();
        var me = this;
        rentalDetailActionView.on(
            {
                delegate: '#backButton',
                tap: function () {
                    me.onGoBack();
                }
            }
        );
        rentalDetailActionView.on(
            {
                itemtap: function (list, index, target, record, e, eOpts) {
                    me.doAction(record);
                }
            }
        );

        var data = [
            {
                description: response[0].description,
                group: 'Description',
                disclosure: false,
                action:'na',
                sortOrder: 1
            }
        ];

        var actionData = null;
        if (response[0].actions) {
            actionData = response[0].actions.map(function (item, index) {
                return {
                    dataBlob: item,
                    actionLabel: item.title,
                    action: item.action,
                    group: 'Actions',
                    disclosure: true,
                    propertyId: response[0].id,
                    sortOrder: 4
                }
            })
        };

        if (response[0].photos) {
            data.push(
                {
                    dataBlob:response[0].photos,
                    actionLabel: 'Photos',
                    action:'view_unit_photos',
                    group: 'Actions',
                    disclosure: true,
                    sortOrder: 4
                }
            )
        };

        if (actionData != null) {
            for (var i=0; i<actionData.length;i++)
            {
                data.push(actionData[i]);
            }
        };

        var store = Ext.create('Ext.data.Store',
            {
                fields: ['dataBlob','actionLabel','action','group','sortOrder','description','disclosure', 'propertyId'],
                data:data
            }
        );

        store.setGrouper(
            {
                sortProperty: 'sortOrder',
                property: 'group'
            }
        )

        rentalDetailActionView.setGrouped(true);
        rentalDetailActionView.setStore(store);

        var me = this;
        rentalDetailActionView.on({
            delegate: '#backButton',
            tap: function () {
                me.onGoBack();
            }
        });

        rentalDetailActionView.setTitle(response[0].name);

        this.getListTitleBarAction().setHidden(true);

        this.doPush(rentalDetailActionView);
    },

    displayReservationDetail:function(record) {

        //
        // Issue, 2 types of reservations. We have currentReservation Object and Primary Reservation Object
        //
        //console.log('displayReservationDetail: ',record);
        var detailView = this.getReservationDetailView();
        var me = this;
        detailView.on({
            delegate: '#backButton',
            tap: function () {
                me.onGoBack();
            }
        });
        detailView.on(
            {
                itemtap: function (list, index, target, record, e, eOpts) {
                    me.doAction(record);
                }
            }
        );

        //
        var arrivalDate = null;
        var departureDate = null;
        if (typeof record.data.dataBlob.currentReservation === "undefined")
        {
            arrivalDate = record.data.dataBlob.arrival;
        }
        else
        {
            arrivalDate = record.data.dataBlob.currentReservation.arrival;
        }

        if (typeof record.data.dataBlob.currentReservation === "undefined")
        {
            departureDate = record.data.dataBlob.departure;
        }
        else
        {
            departureDate = record.data.dataBlob.currentReservation.departure;
        }

        var isVip = null;
        if (typeof record.data.dataBlob.currentReservation === "undefined")
        {
            isVip = record.data.dataBlob.primaryGuest.isvip;
        }
        else
        {
            isVip = record.data.dataBlob.currentReservation.primaryGuest.isvip;

        }

        var data = [
            {
                type: record.data.type,
                typeDescription: record.data.typeDescription,
                status: record.data.status,
                arrival: arrivalDate,
                departure: departureDate,
                isvip: isVip,
                action: 'none',
                group: 'Guest Info',
                disclosure: false,
                sortOrder: 1
            }
        ];

        var propData = null;
        var notesData = null;
        if (record.data.dataBlob.currentReservation)
        {
            propData =  {
               doorCode: record.data.dataBlob.currentReservation.doorCode,
               propertyAccessInstructions: record.data.dataBlob.currentReservation.propertyAccessInstructions,
               action: 'none',
               group: 'Property Info',
               disclosure: false,
               sortOrder: 2
            };

            data.push(propData);

            if (record.data.dataBlob.currentReservation.primaryGuest && record.data.dataBlob.currentReservation.primaryGuest.notes) {
                notesData = record.data.dataBlob.currentReservation.primaryGuest.notes.map(function (item, index) {
                    return {
                        dataBlob: item,
                        title: item.title,
                        text: item.text,
                        group: 'Notes',
                        disclosure: false,
                        sortOrder: 3
                    }
                });
            };
        };

        var actionData = null;
        if (record.data.dataBlob.actions) {
            actionData = record.data.dataBlob.actions.map(function (item, index) {
                return {
                    dataBlob: item,
                    actionLabel: item.title,
                    action: item.action,
                    group: 'Actions',
                    disclosure: false,
                    sortOrder: 10
                }
            });
        };

        if (notesData != null) {
            for (var i=0; i<notesData.length;i++)
            {
                data.push(notesData[i]);
            }
        };

        if (actionData != null) {
            for (var i=0; i<actionData.length;i++)
            {
                data.push(actionData[i]);
            }
        };

        var store = Ext.create('Ext.data.Store',
            {
                fields: ['doorCode','propertyAccessInstructions','action','group','disclosure','sortOrder',
                    'dataBlob','actionLabel','action','fullName','email','isvip','status','arrival', 'departure',
                    'typeDescription','title','text','type'
                ],
                data:data
            }
        );

        store.setGrouper (
            {
                sortProperty: 'sortOrder',
                property: 'group'
            }
        );

        detailView.setGrouped(true);
        detailView.setStore(store);

        detailView.setTitle(record.data.firstName +' ' + record.data.lastName);

        this.getListTitleBarRes().setHidden(true);

        this.doPush(detailView);
    },

    displayRentalDetailActions: function(record) {
        GladStaff.app.showBusy();
        this.getRentalDetailActions(record.data.dataBlob.data);
    },

    displayUnitMap: function(record) {

        var mapUnit = this.getMap();
        var center =  new google.maps.LatLng(record.data.dataBlob.lat, record.data.dataBlob.lng);
        mapUnit.setMapCenter(center);
        mapUnit.addMarker(record.data.dataBlob.lat, record.data.dataBlob.lng, record.data.dataBlob.street);
        var me = this;
        mapUnit.on({
            delegate: '#backButton',
            tap: function () {
                me.onGoBack();
            }
        });

        mapUnit.setTitle('Map');

        this.doPush(mapUnit);
    },

    displayUnitPhotos: function(record) {
        console.log('displayUnitPhotos',record);

        var photoViewer = this.getPhotoViewer();
        var me = this;
        var photoViewerDV = this.getPhotoViewerDataView();
        photoViewerDV.setData(record.data.dataBlob);
        photoViewerDV.addListener(
            {
                selectionchange: function (model, recs) {
                    me.loadCarousel(model, recs);
                }
            }
        );
        photoViewer.addListener(
            {
                delegate: '#backButton',
                tap: function () {
                    me.onGoBack();
                }
            }
        );

        photoViewer.setTitle('Photos');
        this.doPush(photoViewer);
    },

    loadCarousel: function(model, record) {
        var photos = this.getPhotoCarouselView();

        var me = this;
        photos.on({
            delegate: '#backButton',
            tap: function () {
                me.onGoBack();
            }
        });

        var photoArray = model.getData().map(function (item, index) {
            return {
                xtype: 'image',
                src: item.full
            }
        });

        var found = 0;
        for (var i=0; i<photoArray.length;i++)
        {
            if (photoArray[i].src == record[0].data.full){
                found = i;
                break;
            }
        }
        photos.setItems(photoArray);
        photos.setActiveItem(found);

        photos.setTitle('Photos');
        this.doPush(photos);

    },

    // This is called for this.doAction() to display list of Rentals
    displayRentalListDisclose: function(record) {
        GladStaff.app.showBusy();
        this.getAllRentalListData(record.data.path);

    },

    // FIXME - This is really cateogries, not Areas.
    displayAreaList: function(record) {

        var categoryList = this.getRentalCategoryListPanel();
        var me = this;
        categoryList.on(
            {
                delegate: '#backButton',
                tap: function () {
                    me.onGoBack();
                }
            }
        );
        categoryList.on(
            {
                itemtap: function (list, index, target, record, e, eOpts) {
                    me.doAction(record);
                }
            }
        );
        categoryList.setData(record.data.categories);

        categoryList.getStore().sort('category');

        categoryList.setTitle('Unit Categories');
        this.doPush(categoryList);

    },

    // ItemTap Performed on Rental/Property List to Select Specific Unit
    displayRentalUnitDetail: function(path){
        GladStaff.app.showBusy();
        this.getRentalData(path);

    },

    onRentalActionItemDisclose: function(record) {
        console.log('onRentalActionItemDisclose',record);

    },

    search: function(searchField, e, options) {

        var searchValue = searchField.getValue();

        var listPanel = this.getCurrentRentalListPanel();
        var thisRegEx = new RegExp(searchValue, 'i');

        var store = listPanel.getStore();
        store.clearFilter();

        store.filterBy(function (record) {
            if (thisRegEx.test(record.get('title'))) {
                return true;
            };
            return false;
        });

    },

    clearSearch: function(search, e, opts) {
        var listPanel = this.getCurrentRentalListPanel();
        listPanel.getStore().clearFilter();
    },

    searchRentals: function(searchField, e, options) {

        var searchValue = searchField.getValue();

        var listPanel = this.getRentalList();
        var thisRegEx = new RegExp(searchValue, 'i');

        var store = listPanel.getStore();
        store.clearFilter();

        store.filterBy(function (record) {
            if (thisRegEx.test(record.get('name'))) {
                return true;
            };
            return false;
        });

    },

    clearRentalSearch: function(search, e, opts) {
        var listPanel = this.getRentalList();
        listPanel.getStore().clearFilter();
    },

    searchAreas: function(searchField, e, options) {

        var searchValue = searchField.getValue();

        var listPanel = this.getRentalCategoryListPanel();
        var thisRegEx = new RegExp(searchValue, 'i');

        var store = listPanel.getStore();
        store.clearFilter();

        store.filterBy(function (record) {
            if (thisRegEx.test(record.get('category'))) {
                return true;
            };
            return false;
        });

    },

    clearAreaSearch: function(search, e, opts) {
        var listPanel = this.getRentalCategoryListPanel();
        listPanel.getStore().clearFilter();
    },

    updateActions: function(record) {

        var actionSheet = Ext.create('Ext.ActionSheet');
        var me = this;

        var itemActions = [];

        itemActions.push( {
            xtype:'titlebar',
            title: 'Change ' + record.data.actionLabel + ' to',
            ui:'light'
        });

        if (record.data.currentStatus != 'red')
        {
            itemActions.push(
                {
                    cls: 'red-action-button',
                    handler: function ()
                    {
                        me.statusChanged(this, record, 'red',actionSheet, record.data.dataBlob.category);
                    }
                }
            );
        };
        if (record.data.currentStatus != 'yellow')
        {
            itemActions.push(
                {
                    cls: 'yellow-action-button',
                    handler: function ()
                    {
                        me.statusChanged(this, record, 'yellow',actionSheet, record.data.dataBlob.category);
                    }
                }
            );
        };
        if (record.data.currentStatus != 'green')
        {
            itemActions.push(
                {
                    cls:'green-action-button',
                    handler: function ()
                    {
                        me.statusChanged(this, record, 'green',actionSheet, record.data.dataBlob.category);
                    }
                }
            );
        };
        itemActions.push (
            {
                ui:'plain',
                text: 'CANCEL',
                handler: function ()
                {
                    me.statusChanged(this, record, 'cancel', actionSheet, 'cancel');
                }
            }
        );

        actionSheet.setItems(itemActions);

        this.getMainView().add(actionSheet);
        actionSheet.show();

    },

    statusChanged: function (sender, record, newStatus, actionSheet, category){

        if (newStatus == 'cancel'){
            actionSheet.hide();
        }
        else
        {
            var params = {
                    propertyId: record.data.propertyId,
                    newStatus: newStatus,
                    category: category
                };
            GladStaff.app.showBusy();
            GladStaff.backend.updateAction(params, {
                onSuccess: function (response) {
                    GladStaff.app.hideBusy();
                    if (response.success === true) {
                        var actionItems = [];
                        actionSheet.setItems(actionItems);
                        actionSheet.hide();

                        //
                        // how to refresh properly
                        //
                        var path = this.getRentalDetailView().getPropertyDetailPath();
                        this.getMainView().remove(this.getRentalDetailList());
                        //this.getRentalData(path);
                    } else {
                        GladStaff.app.showError(response.status.description);
                    }
                },
                onFailure: function () {
                    console.log('failure');
                    GladStaff.app.hideBusy();
                },
                scope: this
            });
        }
    }
});
