Ext.define('GladStaff.mixin.Maskable', {
    extend: 'Ext.mixin.Mixin',

    requires: ['Ext.Mask'],

    isMaskable: true,

    mixinId: 'maskable',

    statics: {
        LOADING_TEXT:      'Loading...'
    },

    showBusy: function (message, cmp) {
        var me = this,
            masked;

        message = message || me.statics().LOADING_TEXT;
        cmp = cmp || Ext.Viewport;

        masked = cmp.getMasked();

        if (masked && Ext.Viewport.getMasked() instanceof Ext.Mask) {
            masked.setMessage(message);
        } else {
            cmp.mask({
                xtype: 'loadmask',
                message: message
            });
        }
    },

    hideBusy: function (cmp) {
        // use 'null' value instead of 'false' to fully clear masked object
        (cmp || Ext.Viewport).setMasked(null);
    }
});
