Ext.define('GladStaff.view.LoginView', {
    extend: 'Ext.form.Panel',
    xtype: "loginview",

    requires: [
    	'Ext.form.FieldSet',
    	'Ext.form.Password',
    	'Ext.Label',
    	'Ext.Img'
    ],
    config: {
        title: 'Login',
        items: [
        	{
               xtype: 'image',
                //
                // SWITCH TO PROFILES
                //
               src: 'resources/images/gthy-logo.png',
               style: 'width:80px;height:80px;margin:auto'
            },
            {
                xtype: 'label',
                html: 'Login failed. Please enter the correct credentials.',
                itemId: 'signInFailedLabel',
                hidden: true,
                hideAnimation: 'fadeOut',
                showAnimation: 'fadeIn'
            },
            {
                xtype: 'fieldset',
                title: 'GladStaff <sup>TM</sup>',
                items: [
                    {
                        xtype: 'textfield',
                        placeHolder: 'Username',
                        itemId: 'userNameTextField',
                        name: 'userNameTextField',
                        required: true,
                        value: 'mark.gordon@gladtohaveyou.com'
                    },
                    {
                        xtype: 'passwordfield',
                        placeHolder: 'Password',
                        itemId: 'passwordTextField',
                        name: 'passwordTextField',
                        required: true,
                        value: 'ldMKdtmP'
                    }
                ]
            },
            {
                xtype: 'button',
                itemId: 'logInButton',
                ui: 'action',
                padding: '10px',
                text: 'Log In'
            }
        ],
        listeners: [{
    		delegate: '#logInButton',
    		event: 'tap',
    		fn: 'onLogInButtonTap'
		}]
    },
    onLogInButtonTap: function() {

    	var me = this;

    	var usernameField = me.down('#userNameTextField');
    	var passwordField = me.down('#passwordTextField');
    	var label = me.down('#signInFailedLabel');

    	label.hide();

    	var username = usernameField.getValue();
    	var password = passwordField.getValue();

    	var task = Ext.create('Ext.util.DelayedTask', function () {

    		label.setHtml('');
    		me.fireEvent('signInCommand', me, username, password);

    		usernameField.setValue('');
    		passwordField.setValue('');
    	});

    	task.delay(500);
    }
});