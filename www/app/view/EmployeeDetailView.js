Ext.define('GladStaff.view.EmployeeDetailView', {
    extend: 'GladStaff.view.BaseListView',
    xtype: 'employeedetailview',

    requires: [
        'Ext.TitleBar'
    ],
    config: {
        emptyText: 'No employee detail found',
        itemTpl:[
            '<tpl if="this.isActions(group)">',
            '<div class="action-label">{actionLabel}</div>',
            '</tpl>',
            {
                isActions: function(group) {
                    if (group == 'Actions')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        ]
    }
});