Ext.define('GladStaff.model.CurrentRentalModel', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            { name: 'title', type: 'string' },
            { name: 'action', type: 'string' },
            { name: 'group', type: 'string' },
            { name: 'totalProperties', type: 'int'},
            { name: 'path', type: 'string'},
            {name: 'categories', type: 'auto'},
            {name: 'sortOrder', type: 'int'},
            {name: 'dataBlob', type:'auto'},
            {name: 'apiKey', type:'string'}
        ]
    }
});
