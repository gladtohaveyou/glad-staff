Ext.define('GladStaff.view.RentalCategoryListView', {
    extend: 'GladStaff.view.BaseListView',
    xtype: 'rentalcategorylistpanel',

    requires: [
        'Ext.TitleBar'
    ],
    config: {
        emptyText: 'No areas found...',
        grouped:false,
        itemTpl: new Ext.XTemplate(
            '<tpl for=".">',
            '<div>{category}</div>',
            '</tpl>'
        )
    }
});