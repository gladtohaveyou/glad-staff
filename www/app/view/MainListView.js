Ext.define('GladStaff.view.MainListView', {
    extend: 'GladStaff.view.BaseListView',
    xtype: 'mainlistview',

    requires: [
    ],
    config: {
        indexBar: true,
        emptyText: 'No list entries found...',
        grouped:false,
        indexBar:false,
        itemTpl: new Ext.XTemplate(
            '<table width="100%">',
            '<tr><td><div class="{categoryIcon}"></td><td align="right" style="padding-right: 30px;"><div class="category-title">{categoryTitle}</div></td></tr>',
            '<tr><td><div class="category-name">{categoryName}</div><td></td></tr>',
            '</table>'
        ),
        data: [
            {
                "categoryName": "Rental Info",
                "categoryTitle": "View all Rental Info",
                "categoryIcon": "icon-houseCls",
                "categoryOrder": 0,
                "categoryAction": "list-rentals"
            },
            {
                "categoryName": "Staff",
                "categoryTitle": "View all Staff",
                "categoryIcon": "icon-staffCls",
                "categoryOrder": 2,
                "categoryAction": "list-staff"
            },
            {
                "categoryName": "Vendors",
                "categoryTitle": "View all Vendors",
                "categoryIcon": "icon-vendorsCls",
                "categoryOrder": 3,
                "categoryAction": "list-vendors"
            }
        ]
    }
});