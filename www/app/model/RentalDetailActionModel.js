Ext.define('GladStaff.model.RentalDetailActionModel', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            { name: 'title', type: 'string' },
            { name: 'group', type: 'string'},
            { name: 'description', type: 'string'},
            { name: 'actionLabel', type: 'string'},
            { name: 'blob', type: 'auto'},
            { name: 'sortOrder', type:'int'},
            { name: 'action', type:'string'},
            { name: 'propertyId', type:'int'}
        ]
    }
});

