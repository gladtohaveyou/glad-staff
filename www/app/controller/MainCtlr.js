Ext.define('GladStaff.controller.MainCtlr', {
    extend: 'GladStaff.controller.BaseController',
    requires: [
    ],
    config: {
        refs: {
            mainView: 'mainview',
            mainListView : {
                selector: 'mainlistview',
                xtype: 'mainlistview',
                autoCreate: true
            },
            listTitleBar: 'mainlistview #list-title-bar'
        },
        control: {
            mainView: {
                onSignOffCommand: 'onSignOffCommand'
            }
        }
    },

    //called when the Application is launched, remove if not needed
    launch: function(app) {

    },
    index: function(response) {
        GladStaff.app.hideBusy();
        var mainListPanel = Ext.create("GladStaff.view.MainView");

        mainListPanel.setItems(this.onLoadMainMenuList(response.name));
        this.getListTitleBar().setHidden(true);
        Ext.Viewport.animateActiveItem(mainListPanel, this.getSlideLeftTransition());

    },
    onLoadMainMenuList: function(name)
    {
        var mainListView = this.getMainListView();
        var me = this;
        mainListView.on({
            itemtap: function (list, index, target, record, e, eOpts) {
                me.onDisplayList(list, record);
            }
        });

        mainListView.setTitle(name);

        return mainListView;
    },
    onSignOffCommand: function(sender)
    {
        GladStaff.backend.auth = null;
        Ext.Viewport.remove(this.getMainView(),true);
        GladStaff.app.dispatch({
            controller: 'LoginCtlr',
            action: 'index',
            args: []
        });
    },
    onDisplayList: function(sender, record)
    {

        if (record.data.categoryAction == 'list-staff'){
            GladStaff.app.showBusy();
            GladStaff.app.dispatch({
                controller: 'EmployeeCtlr',
                action: 'index',
                args: []
            });
        };
        if (record.data.categoryAction == 'list-vendors'){
            GladStaff.app.showBusy();
            GladStaff.app.dispatch({
                controller: 'VendorCtlr',
                action: 'index',
                args: []
            });
        }
        if (record.data.categoryAction == 'list-guests'){
            GladStaff.app.showBusy();
            GladStaff.app.dispatch({
                controller: 'GuestCtlr',
                action: 'index',
                args: []
            });
        }
        if (record.data.categoryAction == 'list-rentals'){
            GladStaff.app.showBusy();
            GladStaff.app.dispatch({
            controller: 'RentalCtlr',
            action: 'index',
            args: []
        });
    }
    }
});
