Ext.define('GladStaff.view.BaseListView', {
    extend: 'Ext.List',
    xtype: 'baselistview',

    requires: [
        'Ext.TitleBar'
    ],
    config: {
        title: null,
        emptyText: 'no information found',
        onItemDisclosure: true,
        height: 'auto',
        preventSelectionOnDisclose: false,
        variableHeights: false,
        grouped:true,
        indexBar: false,
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                centered: true,
                itemId: 'list-title-bar',
                hidden:false,
                items: [
                    {
                        xtype: 'searchfield',
                        placeHolder: 'search',
                        itemId: 'search-field',
                        centered:true
                    }
                ]
            }
        ]
    }
});