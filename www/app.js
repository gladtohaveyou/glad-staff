/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

Ext.application({
    name: 'GladStaff',

    requires: [
        'GladStaff.backend.Api',
        'GladStaff.backend.Auth',
        'GladStaff.mixin.Maskable',
        'GladStaff.mixin.Messageable',
        'Ext.XTemplate',
        'Ext.data.Store',
        'Ext.util.DelayedTask',
        'Ext.field.Search',
        'Ext.dataview.DataView',
        'Ext.ActionSheet'
    ],

    views: [
        'LoginView',
        'EmployeeListView',
        'ReservationListView',
        'VendorListView',
        'VendorDetailView',
        'EmployeeDetailView',
        'ReservationDetailView',
        'RentalCurrentListView',
        'RentalListView',
        'RentalCategoryListView',
        'RentalDetailView',
        'MapView',
        'RentalDetailActionView',
        'PhotoViewer',
        'PhotoCarouselView',
        'MainView',
        'BaseListView',
        'MainListView',
        'RentalResListView'
    ],

    models: [
        'ReservationModel',
        'CurrentRentalModel',
        'RentalDetailModel',
        'RentalListModel'
    ],

    controllers: [
        'LoginCtlr',
        'MainCtlr',
        'EmployeeCtlr',
        'ReservationCtlr',
        'VendorCtlr',
        'RentalCtlr'
    ],

    stores: [
    ],

    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    launch: function() {
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        // Initialize Mixins
       Ext.app.Application.mixin('messageable',GladStaff.mixin.Messageable);
       Ext.app.Application.mixin('maskable',GladStaff.mixin.Maskable);

        if (Ext.os.is.Android) {
            Ext.Viewport.setAutoBlurInput(true);
        }
        // Adjust toolbar height when running in iOS to fit with new iOS 7 style
        if (Ext.os.is.iOS && Ext.os.version.major >= 7) {
            Ext.select("#ext-viewport").applyStyles("padding-top: 20px;");
        }

        this.onMainLaunch();
        //document.addEventListener('deviceready', this.onMainLaunch, false);

    },

    onMainLaunch: function() {
        GladStaff.app.dispatch({
            controller: 'LoginCtlr',
            action: 'index',
            args: []
        });
    },
    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
