/**
 * A Messageable mixin of the application.
 */
Ext.define('GladStaff.mixin.Messageable', {

    extend: 'Ext.mixin.Mixin',

    requires: ['Ext.MessageBox'],

    isMessageable: true,

    mixinId: 'messageable',

    statics: {
        MESSAGE_TITLE:      'Message',
        INFORMATION_TITLE:  'Information',
        WARNING_TITLE:      'Warning',
        QUESTION_TITLE:     'Question',
        ERROR_TITLE:        'Error',
        CONFIRMATION_TITLE: 'Confirmation'
    },

    msgBoxes: [],

    showMessage: function(message, title, fn, scope, advanced) {
        var me = this,
            msgBox = new Ext.MessageBox();

        advanced = advanced || {};
        Ext.applyIf(advanced, {
            title:          title || me.statics().MESSAGE_TITLE,
            message:        message,
            buttons:        Ext.MessageBox.OK,
            cls:            undefined,
            style: {
                'opacity': '0'
            },
            listeners: {
                hide: function () {
                    // defer destroy() function to properly clear the viewport from the mask
                    Ext.defer(function() {
                        var msg = this.msgBoxes.shift();
                        msg.destroy();
                        msg = null;
                    }, 50, me);
                }
            },
            fn: function() {

                if (fn) {
                    fn.apply(scope, arguments);
                }
            },
            scope: scope || this
        });
        msgBox.show(advanced);

        this.msgBoxes.push(msgBox);

        return msgBox;
    },

    showInfo: function(message, fn, scope, advanced) {
        var me = this;

        advanced = advanced || {};

        Ext.applyIf(advanced, {
            title:  me.statics().INFORMATION_TITLE,
            cls:    Ext.MessageBox.INFO
        });

        me.showMessage(message, null, fn, scope, advanced);
    },

    showWarning: function(message, fn, scope, advanced) {
        var me = this;

        advanced = advanced || {};

        Ext.applyIf(advanced, {
            title:  me.statics().WARNING_TITLE,
            cls:    Ext.MessageBox.WARNING
        });

        me.showMessage(message, null, fn, scope, advanced);
    },

    showQuestion: function(message, fn, scope, advanced) {
        var me = this;

        advanced = advanced || {};

        Ext.applyIf(advanced, {
            title:      me.statics().QUESTION_TITLE,
            buttons:    Ext.MessageBox.YESNO,
            cls:        Ext.MessageBox.QUESTION
        });

        me.showMessage(message, null, fn, scope, advanced);
    },

    showError: function(message, fn, scope, advanced) {
        var me = this;

        advanced = advanced || {};

        Ext.applyIf(advanced, {
            title:  me.statics().ERROR_TITLE,
            cls:    Ext.MessageBox.ERROR
        });

        me.showMessage(message, null, fn, scope, advanced);
    },

    showConfirmation: function(message, fn, scope, advanced, buttons) {
        var me = this;

        advanced = advanced || {};

        Ext.applyIf(advanced, {
            title:      me.statics().CONFIRMATION_TITLE,
            buttons: buttons || Ext.MessageBox.YESNO
        });

        me.showQuestion(message, fn, scope, advanced);
    },

    showPrompt: function(message, fn, scope, advanced, buttons) {
        var me = this;

        advanced = advanced || {};
        Ext.applyIf(advanced, {
            title:      'Prompt',
            buttons:    buttons || Ext.MessageBox.OKCANCEL,
            cls:        'promt-message',//Ext.MessageBox.QUESTION,

            prompt:     {  itemId:'field'},//true,
            multiLine:  false,
            value:      undefined
        });

        me.showMessage(message, null, fn, scope, advanced);
    }
});
