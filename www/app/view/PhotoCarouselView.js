Ext.define('GladStaff.view.PhotoCarouselView', {
    extend: 'Ext.Carousel',
    xtype: 'photocarouselview',

    requires: [

    ],
    config: {
        fullscreen: true,
        layout: 'card',
        title: null
    }
});
